<?php

namespace Drupal\schema;

use Drupal\Core\Extension\ModuleHandler;

/**
 * Since Drupal 10 and ModuleHandler::getImplementations() is removed and
 * ModuleHandler::getImplementationInfo() is protected
 * \Drupal\schema\Plugin\Schema\System cannot call it, we have a child class
 * that will call that method for us.
 */
class SchemaModuleHandler extends ModuleHandler {

  /**
   * @return array
   */
  public function loadSchemaImplementations(): array {
    require_once DRUPAL_ROOT . '/core/includes/common.inc';

    // Load the .install files to get hook_schema.
    $this->loadAllIncludes('install');

    // The previous list of implementation was loaded before the .install files
    // were loaded, reset them so we can get all the schema hooks.
    $this->resetImplementations();

    $schema = [];
    // Invoke hook_schema for all modules.
    if ($this->hasImplementations('schema')) {

      $implementation_info = $this->getImplementationInfo('schema');
      foreach ($implementation_info as $module => $module_file_name) {
        // Cast the result of hook_schema() to an array, as a NULL return value
        // would cause array_merge() to set also the $schema variable to NULL.
        // That would break modules which use $schema further down the line.
        $current = (array) $this->invoke($module, 'schema');
        // Fills in required default values for table definitions from
        // hook_schema(): set 'module' and 'name' keys for each table.
        foreach ($schema as $name => &$table) {
          if (empty($table['module'])) {
            $table['module'] = $module;
          }
          if (!isset($table['name'])) {
            $table['name'] = $name;
          }
        }
        $schema = array_merge($schema, $current);
      }
    }
    $this->alter('schema', $schema);
    return $schema;
  }

}
