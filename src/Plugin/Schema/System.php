<?php

namespace Drupal\schema\Plugin\Schema;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\schema\SchemaProviderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides schema information defined by modules in implementations of
 * hook_schema().
 *
 * Note that we specifically do not use drupal_get_schema() here, because it
 * removes description keys which we want to keep so we can also detect changes
 * on table and column comments during comparison. The downside is that we have
 * to collect all schema information manually ourselves.
 *
 * @SchemaProvider(id = "system")
 */
class System extends PluginBase implements SchemaProviderInterface, ContainerFactoryPluginInterface {

  const CACHE_BIN = 'schema_provider_system';

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ModuleHandlerInterface $moduleHandler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('schema.module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function get($rebuild = FALSE) {
    static $schema;

    if (!isset($schema) || $rebuild) {
      // Try to load the schema from cache.
      if (!$rebuild && $cached = \Drupal::cache()->get(self::CACHE_BIN)) {
        $schema = $cached->data;
      }
      // Otherwise, rebuild the schema cache.
      else {
        $schema = [];

        $schema = $this->moduleHandler->loadSchemaImplementations();

        // If schema is empty, avoid saving it: some database engines require
        // one to perform queries, and this could lead to infinite loops.
        if (!empty($schema)) {
          \Drupal::cache()->set(self::CACHE_BIN, $schema, Cache::PERMANENT);
        }
      }
    }

    return $schema;
  }

}
