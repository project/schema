<?php

namespace Drupal\schema\Comparison;

use Drupal\schema\Comparison\Result\SchemaComparison;

class SchemaComparisonInfoBuilder {
  protected $o;

  public function __construct(SchemaComparison $comparison) {
    $this->o = $comparison;
  }

  public function getInfoArray() {
    return $this->getWarningsArray() + $this->getTablesArray();
  }

  public function getWarningsArray() {
    $info = [];
    $warnings = $this->o->getWarnings();
    if (is_array($warnings)) {
      foreach ($warnings as $warning) {
        $info['warn'][] = $warning;
      }
    }
    return $info;
  }

  public function getTablesArray() {
    $info = [];

    /** @var \Drupal\schema\Comparison\Result\MissingTable $table */
    foreach ($this->o->getMissingTables() as $table) {
      $info['missing'][$table->getModule()][$table->getTableName()] = ['status' => 'missing'];
    }

    /** @var \Drupal\schema\Comparison\Result\TableComparison $table */
    foreach ($this->o->getComparedTables() as $table) {
      $table_info = (new TableComparisonInfoBuilder($table))->getInfoArray();
      $info[$table_info["status"]][$table->getModule()][$table->getTableName()] = $table_info;
    }

    /** @var \Drupal\schema\Comparison\Result\ExtraTable $table */
    foreach ($this->o->getExtraTables() as $table) {
      $info['extra'][] = $table->getTableName();
    }

    return $info;
  }

}
