<?php

namespace Drupal\schema\Comparison\Result;

abstract class BaseColumn {
  protected $columnName;
  protected $tableName;
  protected $schema;

  public function __construct($table_name, $column_name, $schema) {
    $this->tableName = $table_name;
    $this->columnName = $column_name;
    $this->schema = $schema;
  }

  public function getTableName() {
    return $this->tableName;
  }

  public function getColumnName() {
    return $this->columnName;
  }

  public function getModule() {
    if (isset($this->schema['module'])) {
      return $this->schema['module'];
    }
    return 'Unknown';
  }

}
