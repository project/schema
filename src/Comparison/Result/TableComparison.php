<?php

namespace Drupal\schema\Comparison\Result;

class TableComparison {
  const STATUS_SAME = 0;
  const STATUS_DIFFERENT = 1;

  protected $name;
  protected $schema;

  protected $tableCommentDeclared = FALSE;
  protected $tableCommentActual = FALSE;

  protected $fieldsDiff = [];
  protected $fieldsExtra = [];
  protected $fieldsMissing = [];

  protected $indexDiff = [];
  protected $indexExtra = [];
  protected $indexMissing = [];
  protected $status = self::STATUS_SAME;

  public function __construct($name, $schema) {
    $this->name = $name;
    $this->schema = $schema;
  }

  public function getModule() {
    if (isset($this->schema['module'])) {
      return $this->schema['module'];
    }
    return 'Unknown';
  }

  public function isTableCommentDifferent() {
    return $this->getActualTableComment() != $this->getDeclaredTableComment();
  }

  public function getActualTableComment() {
    return $this->tableCommentActual;
  }

  public function getDeclaredTableComment() {
    if (!$this->tableCommentDeclared) {
      $this->tableCommentDeclared = empty($this->schema['description']) ? FALSE : $this->schema['description'];
    }
    return $this->tableCommentDeclared;
  }

  public function setActualTableComment($value) {
    if (empty($value)) {
      $value = FALSE;
    }
    $this->tableCommentActual = $value;
  }

  public function setDeclaredTableComment($value) {
    if (empty($value)) {
      $value = FALSE;
    }
    $this->tableCommentDeclared = $value;
  }

  public function addMissingColumn($field, $definition) {
    $this->status = self::STATUS_DIFFERENT;
    $this->fieldsMissing[$field] = new MissingColumn($this->getTableName(), $field, $definition);
  }

  public function getTableName() {
    return $this->name;
  }

  public function addExtraColumn($field, $schema) {
    $this->status = self::STATUS_DIFFERENT;
    $this->fieldsExtra[$field] = new ExtraColumn($this->getTableName(), $field, $schema);
  }

  public function addColumnDifferences($field, $different_keys, $declared_schema, $actual_schema) {
    $this->status = self::STATUS_DIFFERENT;
    $this->fieldsDiff[$field] = new DifferentColumn($this->getTableName(), $field, $different_keys, $declared_schema, $actual_schema);
  }

  public function addMissingPrimaryKey($definition) {
    $this->addMissingIndex('PRIMARY', 'PRIMARY', $definition);
  }

  public function addMissingIndex($field, $type, $definition) {
    $this->status = self::STATUS_DIFFERENT;
    $this->indexMissing[$field] = new MissingIndex($this->getTableName(), $field, $type, $definition);
  }

  public function addExtraPrimaryKey($definition) {
    $this->addExtraIndex('PRIMARY', 'PRIMARY', $definition);
  }

  public function addExtraIndex($field, $type, $schema) {
    $this->status = self::STATUS_DIFFERENT;
    // Other than for the primary key, this is not necessarily an error as the
    // dba might have added the index on purpose for performance reasons.
    $this->indexExtra[$field] = new ExtraIndex($this->getTableName(), $field, $type, $schema);
  }

  public function addPrimaryKeyDifference($declared, $actual) {
    $this->addIndexDifferences('PRIMARY', 'PRIMARY', $declared, $actual);
  }

  public function addIndexDifferences($field, $type, $declared_schema, $actual_schema) {
    $this->status = self::STATUS_DIFFERENT;
    $this->indexDiff[$field] = new DifferentIndex($this->getTableName(), $field, $type, $declared_schema, $actual_schema);
  }

  public function isStatusSame() {
    return $this->getStatus() == self::STATUS_SAME;
  }

  public function getStatus() {
    return $this->status;
  }

  public function isStatusDifferent() {
    return $this->getStatus() == self::STATUS_DIFFERENT;
  }

  public function getDifferentColumns() {
    return $this->fieldsDiff;
  }

  public function getExtraColumns() {
    return $this->fieldsExtra;
  }

  public function getMissingColumns() {
    return $this->fieldsMissing;
  }

  public function getDeclaredPrimaryKey() {
    return $this->schema['primary key'] ?? FALSE;
  }

  /**
   * Get all declared indexes for this table.
   *
   * @param bool $include_extra
   *   Also include extra indexes, i.e. indexes which are present in the
   *   database but missing from the declared schema.
   *
   * @return array
   */
  public function getDeclaredIndexes($include_extra = FALSE) {
    $indexes = [
      'indexes' => $this->schema['indexes'] ?? [],
      'unique keys' => $this->schema['unique keys'] ?? [],
    ];
    if ($include_extra) {
      /** @var ExtraIndex $index */
      foreach ($this->getExtraIndexes() as $index) {
        $type = $index->getType() == 'UNIQUE' ? 'unique keys' : 'indexes';
        $indexes[$type][$index->getIndexName()] = $index->getSchema();
      }
    }
    return $indexes;
  }

  public function getExtraIndexes() {
    return $this->indexExtra;
  }

  public function getMissingIndexes() {
    return $this->indexMissing;
  }

  public function getDifferentIndexes() {
    return $this->indexDiff;
  }

}
