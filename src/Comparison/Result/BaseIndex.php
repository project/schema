<?php

namespace Drupal\schema\Comparison\Result;

abstract class BaseIndex {

  protected $tableName;
  protected $indexName;
  protected $indexType;
  protected $schema;

  public function __construct($table_name, $index_name, $index_type, $schema) {
    $this->tableName = $table_name;
    $this->indexName = $index_name;
    $this->indexType = $index_type;
    $this->schema = $schema;
  }

  public function getType() {
    return $this->indexType;
  }

  public function getTableName() {
    return $this->tableName;
  }

  public function getIndexName() {
    return $this->indexName;
  }

  public function isPrimary() {
    return $this->indexType == 'PRIMARY';
  }

  public function getModule() {
    if (isset($this->schema['module'])) {
      return $this->schema['module'];
    }
    return 'Unknown';
  }

}
