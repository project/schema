<?php

namespace Drupal\schema\Comparison\Result;

/**
 * Stores the results of a schema comparison.
 *
 * @see Drupal\schema\Comparison\SchemaComparator
 */
class SchemaComparison {
  protected $warnings;

  protected $tablesExtra = [];
  protected $tablesMissing = [];
  protected $tablesCompared = [];

  public function addWarning($warning) {
    $this->warnings[] = $warning;
  }

  public function addMissingTable($name, $definition) {
    $this->tablesMissing[$name] = new MissingTable($name, $definition);
  }

  public function addExtraTable($name, $schema) {
    $this->tablesExtra[$name] = new ExtraTable($name, $schema);
  }

  /**
   * @param $name
   * @param null $schema
   * @return TableComparison
   */
  public function getTableComparison($name, $schema = NULL) {
    if (!isset($this->tablesCompared[$name])) {
      $this->tablesCompared[$name] = new TableComparison($name, $schema);
    }
    return $this->tablesCompared[$name];
  }

  public function getTableNames() {
    $tables = array_merge(
      array_keys($this->tablesExtra),
      array_keys($this->tablesMissing),
      array_keys($this->tablesCompared)
    );
    return $tables;
  }

  public function getComparedTables() {
    return $this->tablesCompared;
  }

  public function getSameTables() {
    return array_filter($this->tablesCompared, function ($table) {
      /** @var TableComparison $table */
      return $table->isStatusSame();
    });
  }

  public function getDifferentTables() {
    return array_filter($this->tablesCompared, function ($table) {
      /** @var TableComparison $table */
      return $table->isStatusDifferent();
    });
  }

  public function getWarnings() {
    return $this->warnings;
  }

  public function getMissingTables() {
    return $this->tablesMissing;
  }

  public function getExtraTables() {
    return $this->tablesExtra;
  }

}
