<?php

namespace Drupal\schema\Comparison\Result;

class MissingIndex extends BaseIndex {

  public function getSchema() {
    return $this->schema;
  }

}
