<?php

namespace Drupal\schema\Comparison\Result;

abstract class BaseTable {
  protected $tableName;
  protected $schema;

  public function __construct($table_name, $schema) {
    $this->tableName = $table_name;
    $this->schema = $schema;
  }

  public function getTableName() {
    return $this->tableName;
  }

  public function getModule() {
    if (isset($this->schema['module'])) {
      return $this->schema['module'];
    }
    return 'Unknown';
  }

}
