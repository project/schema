<?php

namespace Drupal\schema\Comparison\Result;

class DifferentColumn extends BaseColumn {
  protected $differentKeys;
  protected $actualSchema;

  public function __construct($table_name, $column_name, $different_keys, $declared_schema, $actual_schema) {
    parent::__construct($table_name, $column_name, $declared_schema);
    $this->differentKeys = $different_keys;
    $this->actualSchema = $actual_schema;
  }

  public function getActualSchema() {
    return $this->actualSchema;
  }

  public function getDeclaredSchema() {
    return $this->schema;
  }

  public function getDifferentKeys() {
    return $this->differentKeys;
  }

}
