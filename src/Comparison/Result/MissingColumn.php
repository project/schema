<?php

namespace Drupal\schema\Comparison\Result;

class MissingColumn extends BaseColumn {

  public function getSchema() {
    return $this->schema;
  }

}
