<?php

namespace Drupal\schema\Comparison\Result;

class DifferentIndex extends BaseIndex {
  protected $actualSchema;

  public function __construct($table_name, $index_name, $index_type, $declared_schema, $actual_schema) {
    parent::__construct($table_name, $index_name, $index_type, $declared_schema);
    $this->actualSchema = $actual_schema;
  }

  public function getDeclaredSchema() {
    return $this->schema;
  }

  public function getActualSchema() {
    return $this->actualSchema;
  }

}
