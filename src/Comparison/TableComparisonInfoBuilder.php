<?php

namespace Drupal\schema\Comparison;

use Drupal\schema\Comparison\Result\TableComparison;

class TableComparisonInfoBuilder {
  protected $o;

  public function __construct(TableComparison $comparison) {
    $this->o = $comparison;
  }

  public function getInfoArray() {
    $reasons = [];
    $notes = [];

    if ($this->o->isTableCommentDifferent()) {
      $reasons[] = [
        '#markup' => 'Table comment is different.<br/>declared: ' . $this->o->getDeclaredTableComment() .
        '<br/>actual: ' . $this->o->getActualTableComment(),
      ];
    }

    /** @var \Drupal\schema\Comparison\Result\MissingColumn $column */
    foreach ($this->o->getMissingColumns() as $column) {
      $reasons[] = sprintf("%s: not in database", $column->getColumnName());
    }

    /** @var \Drupal\schema\Comparison\Result\DifferentColumn $column */
    foreach ($this->o->getDifferentColumns() as $column) {
      $colname = $column->getColumnName();
      $kdiffs = $column->getDifferentKeys();
      $reasons[] = [
        '#markup' => "column $colname - difference" .
        (count($kdiffs) > 1 ? 's' : '') . " on: " .
        implode(', ', $kdiffs) .
        "<br/>declared: " . schema_phpprint_column($column->getDeclaredSchema()) .
        '<br/>actual: ' . schema_phpprint_column($column->getActualSchema()),
      ];
    }

    /** @var \Drupal\schema\Comparison\Result\ExtraColumn $column */
    foreach ($this->o->getExtraColumns() as $column) {
      $reasons[] = sprintf("%s: unexpected column in database", $column->getColumnName());
    }

    /** @var \Drupal\schema\Comparison\Result\MissingIndex $index */
    foreach ($this->o->getMissingIndexes() as $index) {
      $keyname = $index->getIndexName();
      $type = $index->getType();
      if ($index->isPrimary()) {
        $reasons[] = "primary key: missing in database";
      }
      else {
        $reasons[] = "$type $keyname: missing in database";
      }
    }

    /** @var \Drupal\schema\Comparison\Result\DifferentIndex $index */
    foreach ($this->o->getDifferentIndexes() as $index) {
      $type = $index->getType();
      $keyname = $index->getIndexName();
      if ($index->isPrimary()) {
        $reasons[] = [
          '#markup' => "$type $keyname:<br />declared: " .
          schema_phpprint_key($index->getDeclaredSchema()) . '<br />actual: ' .
          schema_phpprint_key($index->getActualSchema()),
        ];
      }
      else {
        $reasons[] = [
          '#markup' => "primary key:<br />declared: " .
          schema_phpprint_key($index->getDeclaredSchema()) . '<br />actual: ' .
          schema_phpprint_key($index->getActualSchema()),
        ];
      }
    }

    /** @var \Drupal\schema\Comparison\Result\ExtraIndex $index */
    foreach ($this->o->getExtraIndexes() as $index) {
      $keyname = $index->getIndexName();
      $type = $index->getType();
      if ($index->isPrimary()) {
        $reasons[] = "primary key: missing in schema";
      }
      else {
        $notes[] = "$type $keyname: unexpected (not an error)";
      }
    }

    return [
      'status' => $this->o->isStatusDifferent() ? "different" : "same",
      'reasons' => $reasons,
      'notes' => $notes,
    ];
  }

}
